package config

import (
	"errors"
	"path/filepath"
	"strings"

	"github.com/hashicorp/terraform/configs"
	"github.com/hashicorp/terraform/configs/configload"
)

type Candidates map[string]Candidates

func loadModuleDir(dir string) (*configs.Config, error) {
	dir = filepath.Clean(dir)
	loader, err := configload.NewLoader(&configload.Config{
		ModulesDir: filepath.Join(dir, ".terraform/modules"),
	})
	if err != nil {
		return nil, err
	}
	config, diags := loader.LoadConfig(dir)
	if diags.HasErrors() {
		return config, errors.New(diags.Error())
	}
	return config, nil
}

func LoadModule(path string) Candidates {
	root, err := loadModuleDir(path)
	if err != nil {
		panic(err)
	}

	cans := Candidates{}
	mod := root.Module

	// Handle root module attributes/blocks
	for k, _ := range mod.ManagedResources {
		cans[k] = Candidates{}
	}
	for k, _ := range mod.DataResources {
		cans[k] = Candidates{}
	}
	for k, _ := range mod.Locals {
		if _, ok := cans["local"]; !ok {
			cans["local"] = Candidates{}
		}
		cans["local"][k] = Candidates{}
	}
	for k, _ := range mod.ProviderConfigs {
		if _, ok := cans["provider"]; !ok {
			cans["provider"] = Candidates{}
		}
		cans["provider"][k] = Candidates{}
	}
	for k, _ := range mod.Variables {
		if _, ok := cans["var"]; !ok {
			cans["var"] = Candidates{}
		}
		cans["var"][k] = Candidates{}
	}

	// Handle child module output variables
	for _, m := range root.AllModules() {
		if m.Depth() == 0 {
			// Not concerned with root module outputs
			continue
		}
		var prefix Candidates
		if _, ok := cans["module"]; !ok {
			cans["module"] = Candidates{}
		}
		if _, ok := cans["module"][m.Path.String()]; !ok {
			cans["module"][m.Path.String()] = Candidates{}
		}
		prefix = cans["module"][m.Path.String()]
		for k, _ := range m.Module.Outputs {
			prefix[k] = Candidates{}
		}
	}
	return cans
}

func class(attr string) string {
	split := strings.Split(attr, ".")
	switch class := split[0]; class {
	case "data", "local", "provider", "var", "module":
		return class
	}
	return "resource"
}

func typeOf(attr string) string {
	split := strings.Split(attr, ".")
	if len(split) < 2 {
		return ""
	}
	switch class(attr) {
	case "local", "provider", "var":
		return ""
	case "resource":
		return split[0]
	case "data", "module":
		return split[1]
	}
	return "unhandled_type"
}

func name(attr string) string {
	split := strings.Split(attr, ".")
	if len(split) < 2 {
		return ""
	}
	for i, v := range split {
		if v == class(attr) || v == typeOf(attr) {
			continue
		}
		return strings.Join(split[i:], ".")
	}
	return ""
}

func (c Candidates) WithPrefix(p string) Candidates {
	filteredCans := Candidates{}
	if p == "" {
		return c
	}
	path := strings.Split(p, ".")
	for k, v := range c {
		if strings.HasPrefix(k, path[0]) {
			filteredCans[k] = v
			break
		}
	}
	return filteredCans
}

func (c Candidates) Addrs() []string {
	flat := []string{}
	for attr, can := range c {
		if can != nil && len(can) > 0 {
			flat = append(flat, can.flattenChild(attr)...)
		}
	}
	return flat
}

func (c Candidates) flattenChild(parent string) []string {
	flat := []string{}
	for attr, can := range c {
		prefix := parent + "." + attr
		if name(prefix) != "" {
			flat = append(flat, prefix)
		}
		if can != nil && len(can) > 0 {
			flat = append(flat, can.flattenChild(prefix)...)
		}
	}
	return flat
}
