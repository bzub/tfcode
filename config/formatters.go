package config

import (
	"fmt"
	"io"
)

type Formatter func(w io.Writer, cans Candidates)

var Formatters = map[string]Formatter{
	"json":  JsonFormat,
	"nice":  NiceFormat,
	"vim":   vimFormat,
	"array": arrayFormat,
}

func NiceFormat(w io.Writer, cans Candidates) {
	if cans == nil || len(cans) == 0 {
		fmt.Fprintf(w, "Nothing to complete.\n")
		return
	}

	flat := cans.Addrs()
	fmt.Fprintf(w, "Found %d candidates:\n", len(flat))
	for _, path := range flat {
		fmt.Fprintf(w, " Class: %s\n", class(path))
		if typeOf(path) != "" {
			fmt.Fprintf(w, "  Type: %s\n", typeOf(path))
		}
		if name(path) != "" {
			fmt.Fprintf(w, "  Name: %s\n", name(path))
		}
	}
}

func vimFormat(w io.Writer, cans Candidates) {
	if cans == nil || len(cans) == 0 {
		_, err := fmt.Fprint(w, "[0, []]")
		if err != nil {
			panic(err)
		}
		return
	}

	flat := cans.Addrs()
	_, err := fmt.Fprintf(w, "[%d, [", len(flat))
	if err != nil {
		panic(err)
	}
	for i, c := range flat {
		if i != 0 {
			_, err = fmt.Fprintf(w, ", ")
			if err != nil {
				panic(err)
			}
		}

		_, err = fmt.Fprintf(w, "{'word': '%s'}", c)
		if err != nil {
			panic(err)
		}
	}
	_, err = fmt.Fprintf(w, "]]")
	if err != nil {
		panic(err)
	}
}

func arrayFormat(w io.Writer, cans Candidates) {
	if cans == nil || len(cans) == 0 {
		_, err := fmt.Fprint(w, "[]")
		if err != nil {
			panic(err)
		}
		return
	}

	flat := cans.Addrs()
	_, err := fmt.Fprintf(w, "[")
	if err != nil {
		panic(err)
	}
	for i, c := range flat {
		if i != 0 {
			_, err = fmt.Fprintf(w, ", ")
			if err != nil {
				panic(err)
			}
		}

		_, err = fmt.Fprintf(w, `'%s'`, c)
		if err != nil {
			panic(err)
		}
	}
	_, err = fmt.Fprintf(w, "]")
	if err != nil {
		panic(err)
	}
}

func JsonFormat(w io.Writer, cans Candidates) {
	if cans == nil || len(cans) == 0 {
		_, err := io.WriteString(w, "[]")
		if err != nil {
			panic(err)
		}
		return
	}

	flat := cans.Addrs()
	json := fmt.Sprintf(`[%d, [`, len(flat))
	for i, attr := range flat {
		if i != 0 {
			json = json + fmt.Sprint(", ")
		}
		if typeOf(attr) != "" {
			json = json + fmt.Sprintf(
				`{"class": "%s", "type": "%s", "name": "%s"}`,
				class(attr), typeOf(attr), name(attr))
		} else {
			json = json + fmt.Sprintf(
				`{"class": "%s", "name": "%s"}`,
				class(attr), name(attr))
		}
	}
	json = json + fmt.Sprint("]]")
	_, err := io.WriteString(w, json)
	if err != nil {
		panic(err)
	}
}
