package nvim

import (
	"fmt"
	"os"
)

func debugf(format string, args ...interface{}) {
	f, err := os.OpenFile("/tmp/tfcode.log", os.O_RDWR|os.O_APPEND, 0660)
	if err != nil {
		panic(err)
	}
	defer f.Close()

	_, err = fmt.Fprintf(f, format, args...)
	if err != nil {
		panic(err)
	}
}
