package nvim

import (
	"log"
	"regexp"

	"github.com/neovim/go-client/nvim"
	"github.com/neovim/go-client/nvim/plugin"
	"gitlab.com/bzub/tfcode/config"
)

const (
	interpolPattern = `.*\${(?P<interpol>\w*)["]?`
	resourcePattern = `^resource\s+"`
)

var interpolRe *regexp.Regexp
var resourceRe *regexp.Regexp
var all config.Candidates

func init() {
	interpolRe = regexp.MustCompile(interpolPattern)
	resourceRe = regexp.MustCompile(resourcePattern)
	plugin.Main(func(p *plugin.Plugin) error {
		p.HandleFunction(&plugin.FunctionOptions{Name: "TFCodeComplete"}, complete)
		return nil
	})
}

func complete(nv *nvim.Nvim, args []interface{}) (interface{}, error) {
	var moduleDir string
	err := nv.Eval("expand('%:p:h')", &moduleDir)
	if err != nil {
		return nil, err
	}
	log.Printf("Using module dir: \"%s\"\n", moduleDir)
	all = config.LoadModule(moduleDir)
	for i, v := range args {
		log.Printf("Arg #%d: %v\n", i, v)
		switch i {
		case 0:
			if findstart, ok := v.(string); ok {
				if findstart == "1" {
					start, err := findStart(nv)
					return start, err
				}
			}
		case 1:
			if base, ok := v.(string); ok {
				return all.WithPrefix(base).Addrs(), nil
			}
		}
	}
	return "Not Complete Func", nil
}

func findStart(nv *nvim.Nvim) (start int, err error) {
	line, err := nv.CurrentLine()
	if err != nil {
		log.Printf("ERROR: nv.CurrentLine(): %s", err)
		return -1, err
	}
	log.Printf("Finding start from line: \"%s\"\n", line)
	loc := interpolRe.FindSubmatchIndex(line)
	if loc != nil && len(loc) >= 4 {
		log.Printf("Found interpolation start at #%d\n", loc[2])
		return loc[2], nil
	}
	loc = resourceRe.FindIndex(line)
	if loc != nil {
		log.Printf("Found resource start at #%d\n", loc[1])
		return loc[1], nil
	}
	log.Printf("No start pattern found.\n")
	return -1, nil
}
